%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% DiNi(2022) %%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%% Function that Read DiniCOM %%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%% M.Spitoni - Created on 09/02/2023 %%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%input: mytable as app.UITable.Data
%output: TE12, TE13 TE23 as Result of processed errors

function [TE12,TE13,TE23] = f_errcalc(mytable) %Errors
    mytable.INDEX = transpose(1:1:height(mytable));
    vars = ["Station", "Point", "Diff12", "Diff13", "Diff23", "INDEX"];
    TLeft = table(mytable.Station,mytable.Point,...
        mytable.Read_1-mytable.Read_2,mytable.Read_1-mytable.Read_3,mytable.Read_2-mytable.Read_3,...
        mytable.INDEX,VariableNames=vars);
    vars=["Diff12", "Diff13", "Diff23"];
    TRight = grpstats(TLeft,"Station","mean",DataVars=vars);
    leftvars = ["Station", "Point", "Diff12", "Diff13", "Diff23", "INDEX"];
    rightvars = ["mean_Diff12","mean_Diff13","mean_Diff23"];
    TJoin = outerjoin(TLeft,TRight,"Type","full","Keys","Station",...
        LeftVariables=leftvars, RightVariables=rightvars);
    TJoin = sortrows(TJoin,"INDEX");
    TE12 = table2array(table(round(TJoin.Diff12-TJoin.mean_Diff12)));
    TE13 = table2array(table(round(TJoin.Diff13-TJoin.mean_Diff13)));
    TE23 = table2array(table(round(TJoin.Diff23-TJoin.mean_Diff23)));
end


